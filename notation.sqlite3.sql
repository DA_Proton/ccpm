BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "users" (
	"id"	INTEGER NOT NULL UNIQUE,
	"firstname"	TEXT NOT NULL,
	"name"	TEXT NOT NULL,
	"annee_naiss"	INTEGER NOT NULL,
	"sexe"	TEXT NOT NULL,
	"categorie_id"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("categorie_id") REFERENCES "categorie"("id")
);
CREATE TABLE IF NOT EXISTS "natationf" (
	"id"	INTEGER NOT NULL UNIQUE,
	"temps"	REAL NOT NULL,
	"18-21"	INTEGER NOT NULL,
	"22-25"	INTEGER NOT NULL,
	"26-29"	INTEGER NOT NULL,
	"30-33"	INTEGER NOT NULL,
	"34-37"	INTEGER NOT NULL,
	"38-41"	INTEGER NOT NULL,
	"42-45"	INTEGER NOT NULL,
	"46-49"	INTEGER NOT NULL,
	"50et+"	INTEGER NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "natationh" (
	"id"	INTEGER NOT NULL UNIQUE,
	"temps"	REAL NOT NULL,
	"18-21"	INTEGER NOT NULL,
	"22-25"	INTEGER NOT NULL,
	"26-29"	INTEGER NOT NULL,
	"30-33"	INTEGER NOT NULL,
	"34-37"	INTEGER NOT NULL,
	"38-41"	INTEGER NOT NULL,
	"42-45"	INTEGER NOT NULL,
	"46-49"	INTEGER NOT NULL,
	"50et+"	INTEGER NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "pompesh" (
	"id"	INTEGER NOT NULL UNIQUE,
	"nb_rep"	INTEGER NOT NULL,
	"18-21"	INTEGER NOT NULL,
	"22-25"	INTEGER NOT NULL,
	"26-29"	INTEGER NOT NULL,
	"30-33"	INTEGER NOT NULL,
	"34-37"	INTEGER NOT NULL,
	"38-41"	INTEGER NOT NULL,
	"42-45"	INTEGER NOT NULL,
	"46-49"	INTEGER NOT NULL,
	"50et+"	INTEGER NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "pompesf" (
	"id"	INTEGER NOT NULL UNIQUE,
	"nb_rep"	INTEGER NOT NULL,
	"18-21"	INTEGER NOT NULL,
	"22-25"	INTEGER NOT NULL,
	"26-29"	INTEGER NOT NULL,
	"30-33"	INTEGER NOT NULL,
	"34-37"	INTEGER NOT NULL,
	"38-41"	INTEGER NOT NULL,
	"42-45"	INTEGER NOT NULL,
	"46-49"	INTEGER NOT NULL,
	"50et+"	INTEGER NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "marche_courseh" (
	"id"	INTEGER NOT NULL UNIQUE,
	"points"	INTEGER NOT NULL,
	"temps_senior"	TEXT NOT NULL,
	"temps_master1"	TEXT NOT NULL,
	"temps_master2"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "marche_coursef" (
	"id"	INTEGER NOT NULL UNIQUE,
	"points"	INTEGER NOT NULL,
	"temps_senior"	TEXT NOT NULL,
	"temps_master1"	TEXT NOT NULL,
	"temps_master2"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "f2400" (
	"id"	INTEGER NOT NULL UNIQUE,
	"temps"	REAL NOT NULL,
	"18-21"	INTEGER NOT NULL,
	"22-25"	INTEGER NOT NULL,
	"26-29"	INTEGER NOT NULL,
	"30-33"	INTEGER NOT NULL,
	"34-37"	INTEGER NOT NULL,
	"38-41"	INTEGER NOT NULL,
	"42-45"	INTEGER NOT NULL,
	"46-49"	INTEGER NOT NULL,
	"50et+"	INTEGER NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "h2400" (
	"id"	INTEGER NOT NULL UNIQUE,
	"temps"	REAL NOT NULL,
	"18-21"	INTEGER NOT NULL,
	"22-25"	INTEGER NOT NULL,
	"26-29"	INTEGER NOT NULL,
	"30-33"	INTEGER NOT NULL,
	"34-37"	INTEGER NOT NULL,
	"38-41"	INTEGER NOT NULL,
	"42-45"	INTEGER NOT NULL,
	"46-49"	INTEGER NOT NULL,
	"50et+"	INTEGER NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "age_ccps" (
	"id"	INTEGER NOT NULL UNIQUE,
	"age"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "age_ccpg" (
	"id"	INTEGER NOT NULL UNIQUE,
	"age"	TEXT NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "categorie" (
	"id"	INTEGER NOT NULL UNIQUE,
	"name"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "notation" (
	"id"	INTEGER NOT NULL UNIQUE,
	"user_id"	INTEGER NOT NULL,
	"date"	INTEGER NOT NULL,
	"age_ccpg"	TEXT NOT NULL,
	"age_ccps"	TEXT NOT NULL,
	"tps_2400"	REAL NOT NULL,
	"tps_natation"	TEXT NOT NULL,
	"nb_pompe"	INTEGER NOT NULL,
	"tps_marche_course"	REAL NOT NULL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (5,'Audrey','Guiot',1980,'féminin','3');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (6,'Johan','Marcillac',1985,'masculin','2');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (7,'Louis-Edouard','Marteau',1990,'masculin','3');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (8,'Gaelle','Blanchong',1977,'féminin','3');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (9,'Yannick','Pierre',1974,'masculin','3');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (10,'Laurent','Demay',1975,'masculin','3');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (11,'Yann','Robert',1991,'masculin','2');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (12,'Anna','Sockeel',1990,'féminin','2');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (13,'Theo','Cassone',1998,'masculin','1');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (14,'Kyllian','Quesada',2000,'masculin','1');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (15,'Mailys','Albert',2000,'féminin','2');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (16,'Jeremy','Laurent',1993,'masculin','2');
INSERT INTO "users" ("id","firstname","name","annee_naiss","sexe","categorie_id") VALUES (17,'Francois','Garcia',1980,'masculin','3');
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (1,1.5,20,20,20,20,20,20,20,20,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (2,1.59,20,19,19,19,19,20,20,20,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (3,2.05,20,18,18,18,18,20,20,20,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (4,2.08,19,18,18,18,18,19,20,20,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (5,2.14,19,17,17,17,17,19,20,20,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (6,2.17,18,17,17,17,17,18,19,20,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (7,2.23,18,16,16,16,16,18,19,20,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (8,2.26,17,16,16,16,16,17,18,19,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (9,2.32,17,15,15,15,15,17,18,19,20);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (10,2.35,16,15,15,15,15,16,17,18,19);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (11,2.41,16,14,14,14,14,16,17,18,19);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (12,2.44,15,14,14,14,14,15,16,17,18);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (13,2.5,15,13,13,13,13,15,16,17,18);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (14,2.53,14,13,13,13,13,14,15,16,17);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (15,2.59,14,12,12,12,12,14,15,16,17);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (16,3.02,13,12,12,12,12,13,14,15,16);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (17,3.08,13,11,11,11,11,13,14,15,16);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (18,3.11,12,11,11,11,11,12,13,14,15);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (19,3.17,12,10,10,10,10,12,13,14,15);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (20,3.26,11,10,10,10,10,11,12,13,14);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (21,3.35,10,10,10,10,10,10,11,12,13);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (22,3.44,10,10,10,10,10,10,10,11,12);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (23,4.02,10,10,10,10,10,10,10,10,11);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (24,'15m apn�e + 85 natation',10,10,10,10,10,10,10,10,10);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (25,'apn�e >=  10m et termine l''�preuve',8,8,8,8,8,8,8,8,8);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (26,'apn�e >=  5m et termine l''�preuve',7,7,7,7,7,7,7,7,7);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (27,'apn�e < 5m et termine l''�preuve',6,6,6,6,6,6,6,6,6);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (28,'15m apn�e mais ne termine pas l''�preuve',4,4,4,4,4,4,4,4,4);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (29,'apn�e >= 10m et ne termine pas l''�preuve',3,3,3,3,3,3,3,3,3);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (30,'apn�e >= 5m et ne termine pas l''�preuve',2,2,2,2,2,2,2,2,2);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (31,'apn�e < 5m et ne termine pas l''�preuve',0,0,0,0,0,0,0,0,0);
INSERT INTO "natationf" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (32,-1.0,0,0,0,0,0,0,0,0,0);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (1,1.3,20,20,20,20,20,20,20,20,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (2,1.39,20,19,19,19,19,20,20,20,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (3,1.45,20,18,18,18,18,20,20,20,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (4,1.48,19,18,18,18,18,19,20,20,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (5,1.54,19,17,17,17,17,19,20,20,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (6,1.57,18,17,17,17,17,18,19,20,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (7,2.03,18,16,16,16,16,18,19,20,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (8,2.06,17,16,16,16,16,17,18,19,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (9,2.12,17,15,15,15,15,17,18,19,20);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (10,2.15,16,15,15,15,15,16,17,18,19);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (11,2.21,16,14,14,14,14,16,17,18,19);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (12,2.24,15,14,14,14,14,15,16,17,18);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (13,2.3,15,13,13,13,13,15,16,17,18);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (14,2.33,14,13,13,13,13,14,15,16,17);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (15,2.39,14,12,12,12,12,14,15,16,17);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (16,2.42,13,12,12,12,12,13,14,15,16);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (17,2.48,13,11,11,11,11,13,14,15,16);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (18,2.51,12,11,11,11,11,12,13,14,15);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (19,2.57,12,10,10,10,10,12,13,14,15);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (20,3.06,11,10,10,10,10,11,12,13,14);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (21,3.15,10,10,10,10,10,10,11,12,13);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (22,3.24,10,10,10,10,10,10,10,11,12);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (23,3.42,10,10,10,10,10,10,10,10,11);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (24,'15m apn�e + 85 natation',10,10,10,10,10,10,10,10,10);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (25,'apn�e >=  10m et termine l''�preuve',8,8,8,8,8,8,8,8,8);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (26,'apn�e >=  5m et termine l''�preuve',7,7,7,7,7,7,7,7,7);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (27,'apn�e < 5m et termine l''�preuve',6,6,6,6,6,6,6,6,6);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (28,'15m apn�e mais ne termine pas l''�preuve',4,4,4,4,4,4,4,4,4);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (29,'apn�e >= 10m et ne termine pas l''�preuve',3,3,3,3,3,3,3,3,3);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (30,'apn�e >= 5m et ne termine pas l''�preuve',2,2,2,2,2,2,2,2,2);
INSERT INTO "natationh" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (31,'apn�e < 5m et ne termine pas l''�preuve',0,0,0,0,0,0,0,0,0);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (1,50,20,20,20,20,20,20,20,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (2,47,20,19,19,19,19,20,20,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (3,45,20,18,18,18,18,20,20,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (4,44,19,18,18,18,18,19,20,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (5,42,19,17,17,17,17,19,20,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (6,41,18,17,17,17,17,18,20,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (7,39,18,16,16,16,16,18,20,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (8,38,17,16,16,16,16,17,19,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (9,36,17,15,15,15,15,17,19,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (10,35,16,15,15,15,15,16,18,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (11,33,16,14,14,14,14,16,18,20,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (12,32,15,14,14,14,14,15,17,19,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (13,30,15,13,13,13,13,15,17,19,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (14,29,14,13,13,13,13,14,16,18,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (15,28,14,12,12,12,12,14,16,18,20);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (16,27,13,11,11,11,11,13,15,17,19);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (17,26,12,11,11,11,11,12,14,16,18);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (18,25,12,10,10,10,10,12,14,16,18);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (19,24,11,9,9,9,9,11,13,15,17);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (20,23,10,9,9,9,9,10,12,14,16);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (21,22,10,8,8,8,8,10,12,14,16);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (22,21,9,7,7,7,7,9,11,13,15);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (23,20,8,7,7,7,7,8,10,12,14);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (24,19,8,6,6,6,6,8,10,12,14);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (25,18,7,5,5,5,5,7,9,11,13);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (26,17,6,5,5,5,5,6,8,10,12);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (27,16,6,4,4,4,4,6,8,10,12);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (28,15,5,3,3,3,3,5,7,9,11);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (29,14,4,3,3,3,3,4,6,8,10);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (30,13,4,2,2,2,2,4,6,8,10);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (31,12,3,1,1,1,1,3,5,7,9);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (32,11,2,1,1,1,1,2,4,6,8);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (33,10,2,0,0,0,0,2,4,6,8);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (34,9,1,0,0,0,0,1,3,5,7);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (35,7,0,0,0,0,0,0,1,3,5);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (36,6,0,0,0,0,0,0,1,3,5);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (37,4,0,0,0,0,0,0,0,2,4);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (38,3,0,0,0,0,0,0,0,1,3);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (39,2,0,0,0,0,0,0,0,0,2);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (40,1,0,0,0,0,0,0,0,0,1);
INSERT INTO "pompesh" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (41,0,0,0,0,0,0,0,0,0,0);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (1,40,20,20,20,20,20,20,20,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (2,39,20,19,19,19,19,20,20,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (3,35,20,18,18,18,18,20,20,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (4,34,19,18,18,18,18,19,20,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (5,32,19,17,17,17,17,19,20,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (6,31,18,17,17,17,17,18,20,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (7,29,18,16,16,16,16,18,20,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (8,28,17,16,16,16,16,17,19,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (9,26,17,15,15,15,15,17,19,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (10,25,16,15,15,15,15,16,18,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (11,23,16,14,14,14,14,16,18,20,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (12,22,15,14,14,14,14,15,17,19,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (13,21,15,13,13,13,13,15,17,19,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (14,20,14,12,12,12,12,14,16,18,20);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (15,19,13,12,12,12,12,13,15,17,19);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (16,18,12,11,11,11,11,12,14,16,18);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (17,17,11,10,10,10,10,11,13,15,17);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (18,16,10,9,9,9,9,10,12,14,16);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (19,15,9,8,8,8,8,9,11,13,15);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (20,14,8,7,7,7,7,8,10,12,14);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (21,13,7,6,6,6,6,7,9,11,13);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (22,12,6,5,5,5,5,6,8,10,12);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (23,11,5,4,4,4,4,5,7,9,11);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (24,10,4,3,3,3,3,4,6,8,10);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (25,9,3,2,2,2,2,3,5,7,9);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (26,8,2,1,1,1,1,2,4,6,8);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (27,7,1,0,0,0,0,1,3,5,7);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (28,6,0,0,0,0,0,0,2,4,6);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (29,5,0,0,0,0,0,0,1,3,5);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (30,4,0,0,0,0,0,0,0,2,4);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (31,3,0,0,0,0,0,0,0,1,3);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (32,2,0,0,0,0,0,0,0,0,2);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (33,1,0,0,0,0,0,0,0,0,1);
INSERT INTO "pompesf" ("id","nb_rep","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (34,0,0,0,0,0,0,0,0,0,0);
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (1,20,'35','40','45');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (2,19,'36','41','46');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (3,18,'37','42','47');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (4,17,'38','43','48');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (5,16,'39','44','49');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (6,15,'40','45','50');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (7,14,'41','46','51');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (8,13,'42','47','52');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (9,12,'43','48','53');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (10,11,'44','49','54');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (11,10,'45','50','55');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (12,9,'46','51','56');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (13,8,'47','52','57');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (14,7,'48','53','58');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (15,6,'49','54','59');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (16,5,'50','55','60');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (17,4,'51','56','61');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (18,3,'52','57','62');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (19,2,'53','58','63');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (20,1,'54','59','64');
INSERT INTO "marche_courseh" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (21,0,'>54','>59','>64');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (1,20,'45','50','55');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (2,19,'46','51','56');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (3,18,'47','52','57');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (4,17,'48','53','58');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (5,16,'49','54','59');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (6,15,'50','55','60');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (7,14,'51','56','61');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (8,13,'52','57','62');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (9,12,'53','58','63');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (10,11,'54','59','64');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (11,10,'55','60','65');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (12,9,'56','61','66');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (13,8,'57','62','67');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (14,7,'58','63','68');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (15,6,'59','64','69');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (16,5,'60','65','70');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (17,4,'61','66','71');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (18,3,'62','67','72');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (19,2,'63','68','73');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (20,1,'64','69','74');
INSERT INTO "marche_coursef" ("id","points","temps_senior","temps_master1","temps_master2") VALUES (21,0,'>64','>69','>74');
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (1,10.05,20,20,20,20,20,20,20,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (2,10.2,20,19,19,19,19,20,20,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (3,10.3,20,18,18,18,18,20,20,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (4,10.35,19,18,18,18,18,19,20,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (5,10.45,19,17,17,17,17,19,20,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (6,10.5,18,17,17,17,17,18,20,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (7,11.0,18,16,16,16,16,18,20,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (8,11.05,17,16,16,16,16,17,19,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (9,11.15,17,15,15,15,15,17,19,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (10,11.2,16,15,15,15,15,16,18,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (11,11.3,16,14,14,14,14,16,18,20,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (12,11.35,15,14,14,14,14,15,17,19,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (13,11.45,15,13,13,13,13,15,17,19,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (14,11.5,14,13,13,13,13,14,16,18,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (15,12.0,14,12,12,12,12,14,16,18,20);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (16,12.05,13,12,12,12,12,13,15,17,19);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (17,12.15,13,11,11,11,11,13,15,17,19);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (18,12.2,12,11,11,11,11,12,14,16,18);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (19,12.3,12,10,10,10,10,12,14,16,18);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (20,12.35,11,10,10,10,10,11,13,15,17);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (21,12.45,11,9,9,9,9,11,13,15,17);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (22,12.5,10,9,9,9,9,10,12,14,16);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (23,13.0,10,8,8,8,8,10,12,14,16);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (24,13.05,9,8,8,8,8,9,11,13,15);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (25,13.15,9,7,7,7,7,9,11,13,15);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (26,13.2,8,7,7,7,7,8,10,12,14);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (27,13.3,8,6,6,6,6,8,10,12,14);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (28,13.35,7,6,6,6,6,7,9,11,13);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (29,13.45,7,5,5,5,5,7,9,11,13);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (30,13.5,6,5,5,5,5,6,8,10,12);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (31,14.0,6,4,4,4,4,6,8,10,12);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (32,14.05,5,4,4,4,4,5,7,9,11);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (33,14.15,5,3,3,3,3,5,7,9,11);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (34,14.2,4,3,3,3,3,4,6,8,10);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (35,14.3,4,2,2,2,2,4,6,8,10);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (36,14.35,3,2,2,2,2,3,5,7,9);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (37,14.45,3,1,1,1,1,3,5,7,9);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (38,14.5,2,1,1,1,1,2,4,6,8);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (39,15.0,2,0,0,0,0,2,4,6,8);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (40,15.05,1,0,0,0,0,1,3,5,7);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (41,15.15,1,0,0,0,0,1,3,5,7);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (42,15.3,0,0,0,0,0,0,2,4,6);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (43,15.45,0,0,0,0,0,0,1,3,5);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (44,16.0,0,0,0,0,0,0,0,2,4);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (45,16.15,0,0,0,0,0,0,0,1,3);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (46,16.3,0,0,0,0,0,0,0,0,2);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (47,16.45,0,0,0,0,0,0,0,0,1);
INSERT INTO "f2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (48,17.0,0,0,0,0,0,0,0,0,0);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (1,8.45,20,20,20,20,20,20,20,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (2,9.0,20,19,19,19,19,20,20,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (3,9.1,20,18,18,18,18,20,20,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (4,9.15,19,18,18,18,18,19,20,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (5,9.25,19,17,17,17,17,19,20,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (6,9.3,18,17,17,17,17,18,20,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (7,9.4,18,16,16,16,16,18,20,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (8,9.45,17,16,16,16,16,17,19,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (9,9.55,17,15,15,15,15,17,19,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (10,10.0,16,15,15,15,15,16,18,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (11,10.1,16,14,14,14,14,16,18,20,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (12,10.15,15,14,14,14,14,15,17,19,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (13,10.25,15,13,13,13,13,15,17,19,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (14,10.3,14,13,13,13,13,14,16,18,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (15,10.4,14,12,12,12,12,14,16,18,20);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (16,10.45,13,12,12,12,12,13,15,17,19);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (17,10.55,13,11,11,11,11,13,15,17,19);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (18,11.0,12,11,11,11,11,12,14,16,18);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (19,11.1,12,10,10,10,10,12,14,16,18);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (20,11.15,11,10,10,10,10,11,13,15,17);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (21,11.25,11,9,9,9,9,11,13,15,17);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (22,11.3,10,9,9,9,9,10,12,14,16);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (23,11.4,10,8,8,8,8,10,12,14,16);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (24,11.45,9,8,8,8,8,9,11,13,15);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (25,11.55,9,7,7,7,7,9,11,13,15);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (26,12.0,8,7,7,7,7,8,10,12,14);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (27,12.1,8,6,6,6,6,8,10,12,14);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (28,12.15,7,6,6,6,6,7,9,11,13);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (29,12.25,7,5,5,5,5,7,9,11,13);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (30,'12.30.',6,5,5,5,5,6,8,10,12);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (31,'12.40.',6,4,4,4,4,6,8,10,12);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (32,12.45,5,4,4,4,4,5,7,9,11);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (33,12.55,5,3,3,3,3,5,7,9,11);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (34,13.0,4,3,3,3,3,4,6,8,10);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (35,13.1,4,2,2,2,2,4,6,8,10);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (36,13.15,3,2,2,2,2,3,5,7,9);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (37,13.25,3,1,1,1,1,3,5,7,9);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (38,13.3,2,1,1,1,1,2,4,6,8);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (39,13.4,2,0,0,0,0,2,4,6,8);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (40,13.45,1,0,0,0,0,1,3,5,7);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (41,13.55,1,0,0,0,0,1,3,5,7);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (42,14.1,0,0,0,0,0,0,2,4,6);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (43,14.25,0,0,0,0,0,0,1,3,5);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (44,14.4,0,0,0,0,0,0,0,2,4);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (45,14.55,0,0,0,0,0,0,0,1,3);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (46,15.1,0,0,0,0,0,0,0,0,2);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (47,15.25,0,0,0,0,0,0,0,0,1);
INSERT INTO "h2400" ("id","temps","18-21","22-25","26-29","30-33","34-37","38-41","42-45","46-49","50et+") VALUES (48,15.4,0,0,0,0,0,0,0,0,0);
INSERT INTO "age_ccps" ("id","age") VALUES (1,'>39 senior');
INSERT INTO "age_ccps" ("id","age") VALUES (2,'40-49 master1');
INSERT INTO "age_ccps" ("id","age") VALUES (3,'50+ master2');
INSERT INTO "age_ccpg" ("id","age") VALUES (1,'18-21');
INSERT INTO "age_ccpg" ("id","age") VALUES (2,'22-25');
INSERT INTO "age_ccpg" ("id","age") VALUES (3,'26-29');
INSERT INTO "age_ccpg" ("id","age") VALUES (4,'30-33');
INSERT INTO "age_ccpg" ("id","age") VALUES (5,'34-37');
INSERT INTO "age_ccpg" ("id","age") VALUES (6,'38-41');
INSERT INTO "age_ccpg" ("id","age") VALUES (7,'42-45');
INSERT INTO "age_ccpg" ("id","age") VALUES (8,'46-49');
INSERT INTO "age_ccpg" ("id","age") VALUES (9,'50+');
INSERT INTO "categorie" ("id","name") VALUES (1,'VT');
INSERT INTO "categorie" ("id","name") VALUES (2,'EVSMA/EVAT');
INSERT INTO "categorie" ("id","name") VALUES (3,'SOFF');
INSERT INTO "categorie" ("id","name") VALUES (4,'OFF');
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (7,5,2020,'38-41','40-49 master1',12.16,'2.14',35,46.2);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (8,5,2020,'38-41','40-49 master1',12.16,'2.14',35,46.2);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (9,5,2020,'38-41','40-49 master1',12.16,'2.14',35,46.2);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (12,7,2020,'30-33','>39 senior',9.35,'1.54',43,44.29);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (13,6,2020,'34-37','>39 senior',9.47,'2.15',50,36.15);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (21,8,2020,'42-45','40-49 master1',12.29,'2.32',35,46.2);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (22,9,2020,'46-49','40-49 master1',-1.0,'1.45',-1,36.27);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (26,10,2020,'46-49','40-49 master1',10.42,'-1',33,36.3);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (27,15,2020,'18-21','>39 senior',14.14,'-1',25,-1.0);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (28,16,2020,'26-29','>39 senior',13.59,'-1',22,46.1);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (29,11,2020,'30-33','>39 senior',-1.0,'1.39',-1,52.5);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (30,12,2020,'30-33','>39 senior',-1.0,'2.05',-1,66.0);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (31,13,2020,'22-25','>39 senior',9.0,'1.39',50,37.2);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (32,14,2020,'18-21','>39 senior',10.04,'-1',30,39.5);
INSERT INTO "notation" ("id","user_id","date","age_ccpg","age_ccps","tps_2400","tps_natation","nb_pompe","tps_marche_course") VALUES (33,17,2021,'42-45','40-49 master1',8.24,'2.3',45,29.19);
COMMIT;
