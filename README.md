# CCPM CALC

__Prérequis__: avoir installé PHP et composer. Composer est un gestionnaire de paquets pour PHP.

## How to use it ?
Depuis un CLI (invite de commandes): 

```
git clone https://gitlab.com/DA_Proton/ccpm.git
cd ccpm
composer install
php -S 127.0.0.1:8000
```

>Dans un navigateur entrer 127.0.0.1:8000 dans la barre d'adresse.