<?php

use \Tamtamchik\SimpleFlash\Flash;

/**
 * Rendu de l'index
 */
function index()
{
    setlocale(LC_TIME, "fr_FR", "French");
    $users = Model::factory('Users')->find_many();
    foreach($users as $user) {
        $notes[] = notesIndex($user->id);
    }
    $data = [
        'notes' => $notes,
        'users' => $users,
    ];
    Flight::render('index.twig', $data);
}

/**
 * rendu du tableauu de bord
 */
function userstab() {
    $data = [
        'users' => Model::factory('Users')->find_many(),
        'categorie' => Model::factory('Categorie')->find_many(),
    ];
    Flight::render('userstab.twig',$data);
}

/**
 * Ajout d'un user
 */
function adduser($id) {
    if(Flight::request()->method == 'POST'){
        $formData = Flight::request()->data;
        if(!$id) {
            $user = Model::factory('Users')->create();
        } else {
            $user = Model::factory('Users')->find_one($id);
        }
        $user->firstname = $formData->firstname;
        $user->name = $formData->name;
        $user->annee_naiss = $formData->annee_naiss;
        $user->sexe = $formData->sexe;
        $user->categorie_id = intval($formData->categorie);
        $user->save();
        Flash::success('L\'utilisateur a été ajouté');
        Flight::redirect('/userstab');
    }
    $data = [
        'categorie' => Model::factory('Categorie')->find_many(),
    ];
    if($id) $data['user'] = Model::factory('Users')->find_one($id);
    Flight::render('adduser.twig', $data);
}

function profile($id) {
    $user = Model::factory('Users')->find_one($id);
    if(!$id or $user->id == null) {
        Flash::warning('Utilisateur introuvable');
        Flight::redirect('/userstab');
        return 0;
    }
    $notes = notes($id);
    $data = [
        'user' => $user,
        'categorie' => Model::factory('Categorie')->find_many(),
        'notes' => $notes,
    ];
    Flight::render('/userprofile.twig', $data);
}

function notesIndex($id) {
    $user = Model::factory('Users')->find_one($id);
    $notation = Model::factory('Notation')->where('user_id', $id)->find_many();
    $notes = [];
    if (isset($notation[0])){
        $nt = array_reverse($notation)[0];
        $notes = [
            'id' => $nt->id,
            'user_id' => $nt->user_id,
            'date' => $nt->date,
            'age_ccpg' => $nt->age_ccpg,
            'age_ccps' => $nt->age_ccps,
            '2400' => calc2400($nt, $user),
            'natation' => calcNatation($nt, $user),
            'pompe' => calcPompe($nt, $user),
            'marcheCourse' => calcMacheCourse($nt, $user),
        ];
    }
    return $notes;
}

function notes($id) {
    $user = Model::factory('Users')->find_one($id);
    $notation = Model::factory('Notation')->where('user_id', $id)->find_many();
    $notes = [];
    foreach($notation as $nt) {
        $note = [
            'id' => $nt->id,
            'user_id' => $nt->user_id,
            'age_ccpg' => $nt->age_ccpg,
            'age_ccps' => $nt->age_ccps,
            '2400' => calc2400($nt, $user),
            'natation' => calcNatation($nt, $user),
            'pompe' => calcPompe($nt, $user),
            'marcheCourse' => calcMacheCourse($nt, $user),
        ];
        $notes[] = $note;
    }
    return $notes;
}

function calc2400($nt, $user) {
    if($user->sexe == 'masculin') {
        $n2400 = Model::factory('H2400')->find_many();
    } else $n2400 = Model::factory('F2400')->find_many();
    if($nt->tps_2400 == -1) {
        return "N/A";
    }
    else if($nt->tps_2400 < 8.45) return 20;
    foreach($n2400 as $el){
        if($el->temps > $nt->tps_2400) {
            $n2400 = $n2400[$el->id - 2]->as_array();
            return $n2400[$nt->age_ccpg];  
        }
    }
    return 0;
}

function calcNatation($nt, $user) {
    if($user->sexe == "masculin") {
        $natation = Model::factory('Natationh')->where('temps',$nt->tps_natation)->find_one();
    } else $natation = Model::factory('Natationf')->where('temps',$nt->tps_natation)->find_one();
    if($nt->tps_natation == "-1") return "N/A";
    else if($nt->tps_natation == "0") return "Erreur";
    $natation = $natation->as_array();
    return $natation[$nt->age_ccpg];
}

function calcPompe($nt, $user) {
    if($user->sexe == 'masculin') {
        $pompe = Model::factory('Pompesh')->find_many();
    } else $pompe = Model::factory('Pompesf')->find_many();
    if($nt->nb_pompe == -1) return "N/A";
    else if($nt->nb_pompe > 50) return 20;
    foreach($pompe as $el) {
        if(intval($nt->nb_pompe) > intval($el->nb_rep)) {
            $pompe = $pompe[$el->id-2]->as_array();
            return $pompe[$nt->age_ccpg];
        } 
    }
    return 0;
}

function calcMacheCourse($nt, $user) {
    if($user->sexe == 'masculin') {
        $mc = Model::factory('Marche_courseh')->find_many();
    } else $mc = Model::factory('Marche_coursef')->find_many();
    if($nt->tps_marche_course == -1) return "N/A"; 
    foreach($mc as $el) {
        if($nt->age_ccps == ">39 senior") {
            if ($user->sexe == "masculin" and $nt->tps_marche_course <= 35) return 20;
            else if($nt->tps_marche_course <= 45) return 20;
            if($el->temps_senior > $nt->tps_marche_course) {
                $mc = $mc[$el->id - 2]->as_array();
                return $mc['points'];
            }
        } else if($nt->age_ccps == "40-49 master1") {
            if ($user->sexe == "masculin" and $nt->tps_marche_course <= 40) return 20;
            else if($nt->tps_marche_course <= 50) return 20;
            if($el->temps_master1 > $nt->tps_marche_course) {
                $mc = $mc[$el->id -2]->as_array();
                return $mc['points'];
            }
        }  else if($nt->age_ccps == "50+ master2") {
            if ($user->sexe == "masculin" and $nt->tps_marche_course <= 45) return 20;
            else if($nt->tps_marche_course <= 55) return 20;
            if($el->temps_master2 > $nt->tps_marche_course) {
                $mc = $mc[$el->id -2]->as_array();
                return $mc['points'];
            }
        }
    }   
    return 0;
}

function notation($userid, $ntid=0) {
    $user = Model::factory('Users')->find_one($userid);
    if(Flight::request()->method == 'POST') {
        $formData = Flight::request()->data;
        addNotation($user, $formData, $ntid);
    }
    $data = [
        'user' => $user,
        'natationf' => Model::factory('Natationf')->find_many(),
        'natationh' => Model::factory('Natationh')->find_many(),
    ];
    if($ntid != 0) $data['notation'] = Model::factory('Notation')->find_one($ntid);
    Flight::render('notation.twig', $data);
}

function addNotation($user, $formData, $id=0) {
    if($id==0) $notation = Model::factory('Notation')->create();
    else $notation = Model::factory('Notation')->find_one($id);
    $notation->user_id = $user->id;
    $notation->date = intval(date('Y'));
    $notation->age_ccpg = calcAgeCcpg($user->annee_naiss);
    $notation->age_ccps = calcAgeCcps($user->annee_naiss);
    $notation->tps_2400 = floatval($formData->tps_2400);
    $notation->tps_natation = $formData->tps_natation;
    $notation->nb_pompe = $formData->nb_pompe;
    $notation->tps_marche_course = $formData->tps_marche_course;
    $notation->save();
    Flash::success('La notation a été ajouté');
    Flight::redirect('/profile/'. $user->id);
}

function calcAgeCcpg($date) {
    $ageusr = intval(date('Y')) - $date;
    $age = Model::factory('Age_ccpg')->find_many();
    for($i=0; $i<8; $i++){
        $tab = explode("-", $age[$i]->age);
        if($ageusr < intval($tab[1])) {
            return $age[$i]->age;
        }
    }
    return $age[8]->age;
}

function calcAgeCcps($date) {
    $ageusr = intval(date('Y')) - $date;
    echo $ageusr;
    $age = Model::factory('Age_ccps')->find_many();
    if($ageusr <= 39) {
        return $age[0]->age;
    } else if($ageusr >= 40 and $ageusr <= 49) {
        return $age[1]->age;
    } else {
        return $age[2]->age;
    }
}