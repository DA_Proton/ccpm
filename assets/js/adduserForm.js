var formvalid = document.getElementById('adduserSubmit');

var nameValid = /^[a-zA-Z]+[ \-']?[[a-zA-Z]+[ \-']?]*[a-zA-Z]+$/
var firstname = document.getElementById('firstname');
var missFirstname = document.getElementById('missFirstname')
var nom = document.getElementById('name');
var missName = document.getElementById('missName')

var anneeValid = /^(19|20)\d{2}$/;
var annee_naiss = document.getElementById('annee_naiss');
var missAnnee = document.getElementById('missAnnee');

formvalid.addEventListener('click', validation);

function validation(event){
    if(anneeValid.test(annee_naiss.value) == false){
        event.preventDefault();
        missAnnee.textContent = 'Format incorrect';
        missAnnee.style.color = 'red';
    } else {}
    if(nameValid.test(firstname.value) == false){
        event.preventDefault();
        missFirstname.textContent = 'Prénom incorrect';
        missFirstname.style.color = 'red';
    } else {}
    if(nameValid.test(nom.value) == false){
        event.preventDefault();
        missName.textContent = 'Nom incorrect';
        missName.style.color = 'red';
    } else{}
    if(document.getElementById('categorie').value == "0"){
        event.preventDefault();
        document.getElementById('missCategorie').textContent = "Veuillez selectionner une catégorie";
        document.getElementById('missCategorie').style.color = 'red'
    } else {}
}

