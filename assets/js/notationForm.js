var patternReal = /-?[0-9]+(.[0-9]+)?/;
var patternInt = /-?[0-9]+/;

var formValid = document.getElementById('addnotationSubmit');

var tps_2400 = document.getElementById('tps_2400');
var missTps_2400 = document.getElementById('missTps_2400');

var tps_natation = document.getElementById('tps_natation');
var missTps_natation = document.getElementById('missTps_natation');

var nb_pompe = document.getElementById('nb_pompe');
var missNb_pompe = document.getElementById('missNb_pompe');

var tps_marche_course = document.getElementById('tps_marche_course');
var missTps_marche_course = document.getElementById('missTps_marche_course');

formValid.addEventListener('click', validation);

function validation(event) {
    if(patternReal.test(tps_2400.value) == false) {
        event.preventDefault();
        missTps_2400.textContent = 'Format incorrect, ex: 2.36';
        missTps_2400.style.color = 'red';
    } else {}
    if(tps_natation.value == "0") {
        event.preventDefault();
        missTps_natation.textContent = 'Veuillez séléctionner une valeur';
        missTps_natation.style.color = 'red';
    } else {}
    if(patternInt.test(nb_pompe.value) == false) {
        event.preventDefault();
        missNb_pompe.textContent = 'Format incorrect, ex: 25';
        missNb_pompe.style.color = 'red';
    } else {}
    if(patternReal.test(tps_marche_course.value) == false) {
        event.preventDefault();
        missTps_marche_course.textContent = 'Format incorrect, ex: 2.36';
        missTps_marche_course.style.color = 'red';
    } else {}
}